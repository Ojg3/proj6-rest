Project 6
Author: Otis Greer
Email: ogreer@uoregon.edu

Description:
This program should get the data from the database created in the previous project and return the desired data (such as all the times, only the open times, and only the close times.). If the user wants the list in a desired format, (such as json or csv) the url ending should determine which format is returned. If the user only wants the top 'k' values in the database, then after the desired formatting, then the set number of times in order will be returned.
